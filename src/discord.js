import http from 'node:http'
import https from 'node:https'
import {do_with_data} from "beastman_js_utils"
import axios from "axios"


const max_message_size = 2_000

export function discord_send(s) {
    return do_with_data("keys.json", async (keys) => {
        const message_chunks = break_into_chunks(max_message_size, "\n", s)
        for(const chunk of message_chunks) {
            await axios.post(`${keys.DISCORD_WEBHOOK_URL}?wait=true`, {
                content: chunk
            },{
                httpsAgent: new https.Agent({keepAlive: false}),
                httpAgent: new http.Agent({keepAlive: false}),
            })
        }
    })
}

function break_into_chunks(chunk_max_size, separator_char, str) {
    const output_chunks = []
    const src_split = str.split(separator_char)
    for(let src_index=0, dst_index=0; src_index<src_split.length;) {
        if(src_split[src_index].length > chunk_max_size) {
            src_index++
            continue
        }
        if(!output_chunks[dst_index]) {
            output_chunks[dst_index] = src_split[src_index]
            src_index++
            continue
        }

        if(output_chunks[dst_index].length + src_split[src_index].length > chunk_max_size) {
            dst_index++
        } else {
            output_chunks[dst_index] += separator_char + src_split[src_index]
            src_index++
        }
    }

    return output_chunks
}
