import cliProgress from "cli-progress"
import { client, getGames } from "cfbd"
import { do_with_data } from "beastman_js_utils";
import {discord_send} from "./discord.js";

const data_filepath = "data.json"
const keys_filepath = "keys.json"

function fetch_model_season(season) {
    console.log("Fetching model season", season)
    return do_with_data(data_filepath, async (data, save) => {
        const updated_season = await getGames({
            query: {
                year: season,
                classification: "fbs"
            }
        })

        data.seasons = data.seasons || {}
        data.seasons[season] = data.seasons[season] || {}

        data.seasons[season].db = updated_season
        data.seasons[season].last_fetched_epoch = Date.now()

        let score_sum = 0
        data.seasons[season].db.data.forEach((game) => {
            if(game.completed) {
                score_sum += game.homePoints;
                score_sum += game.awayPoints;
            }
        })
        data.seasons[season].score_sum = score_sum
        await save()
    })
}
function fetch_latest() {
    return do_with_data(data_filepath, async (data) => {
        const current_date = new Date()
        current_date.setHours(0,0,0,0)
        const current_season = current_date.getFullYear()

        const this_season_last_fetched = data.seasons?.[current_season]?.last_fetched_epoch || 0
        if(this_season_last_fetched < current_date) {
            await fetch_model_season(current_season)
        }
        const last_season_last_fetched = data.seasons?.[current_season-1]?.last_fetched_epoch || 0
        if(current_date.getMonth() < 2 && last_season_last_fetched < current_date) {
            await fetch_model_season(current_season-1)
        }
    })
}

function get_teams_games(season, team) {
    return do_with_data(data_filepath, (data) => {
        const output = []
        for(const game of data.seasons[season].db.data) {
            if(game.homeTeam == team || game.awayTeam == team) {
                output.push(game)
            }
        }
        return output
    })
}
function get_season_teams(season) {
    const all_the_teams_all_the_games = []
    return do_with_data(data_filepath, (data) => {
        data.seasons[season].db.data.forEach((game) => {
            if(game.homeClassification == "fbs") {
                all_the_teams_all_the_games.push(game.homeTeam)
            }
            if(game.awayClassification == "fbs") {
                all_the_teams_all_the_games.push(game.awayTeam)
            }
        })

        const teams_deduped = [...new Set(all_the_teams_all_the_games)]
        return teams_deduped
    })

}
function get_season_team_pfpa(data, season, team) {
    let pf = 1
    let pa = 1

    data.seasons[season].db.data.forEach((game) => {
        if(!game.completed) {
            return
        }

        if(game.homeTeam == team) {
            pf += game.homePoints
            pa += game.awayPoints
        } else if (game.awayTeam == team) {
            pf += game.awayPoints
            pa += game.homePoints
        }
    })

    return pf / pa
}
function season_median_num_games(season) {
    return do_with_data(data_filepath, (data) => {
        let games_per_team = {}
        data.seasons[season].db.data.forEach((game) => {
            games_per_team[game.awayTeam] = ++games_per_team[game.awayTeam] || 1
            games_per_team[game.homeTeam] = ++games_per_team[game.homeTeam] || 1
        })
        let num_games = []
        Object.keys(games_per_team).forEach((team) => {
            num_games.push(Number(games_per_team[team]))
        })
        num_games = num_games.sort((a, b) => a-b)
        return num_games[Math.floor(num_games.length / 2)]
    })
}
function calculate_rankings_for_season(season) {
    return do_with_data(data_filepath, async (data) => {
        if(data.seasons[season].score_sum == data.seasons[season].last_calculated_score_sum) {
            return data.seasons[season].rankings
        } else {
            console.log(`Calculating talent scores for season`, season)
            const list_of_teams = await get_season_teams(season)
            const progress_bar = new cliProgress.SingleBar({
                forceRedraw: true,
                etaAsynchronousUpdate: true,
                format: 'progress [{bar}] {percentage}% | ETA: {eta_formatted} | {value}/{total}',
                barCompleteChar: '=',
                barIncompleteChar: '-'
            })
            progress_bar.start(list_of_teams.length, 0)
            const median_num_games = await season_median_num_games(season)
            const rankings = await Promise.all(
                list_of_teams.map(async (team_name) => {
                    const talent_score = await recursive_calculate_season_team_talent(season, team_name, median_num_games, [])
                    progress_bar.increment()
                    progress_bar.render()
                    return {
                        name: team_name,
                        talent_score: talent_score
                    }
                })
            )
            progress_bar.stop()

            rankings.sort((a, b) =>
                b.talent_score - a.talent_score ||
                get_season_team_pfpa(data, season, b.name) - get_season_team_pfpa(data, season, a.name) ||
                b.name.localeCompare(a.name)
            )

            data.seasons[season].rankings = rankings
            data.seasons[season].last_calculated_score_sum = data.seasons[season].score_sum

            return rankings
        }
    })
}
async function recursive_calculate_season_team_talent(season, team, level, path) {
    if(level == 0) return 0

    let output = 0
    let my_path = [...path]
    my_path.push(team)

    const games_played = await get_teams_games(season, team)
    for(const game of games_played) {
        if(game.completed == false) continue
        const did_win =
            (game.homeTeam==team && game.homePoints>game.awayPoints) ||
            (game.awayTeam==team && game.awayPoints>game.homePoints)
        const other_team = game.homeTeam==team ? game.awayTeam : game.homeTeam

        if(did_win) {
            output += 1
            if(!my_path.includes(other_team)) {
                output += await recursive_calculate_season_team_talent(season, other_team, level-1, my_path)
            }
        }
    }
    return output
}
function rankings_to_string(season) {
    return do_with_data(data_filepath, async (data) => {
        const rankings = data.seasons[season].rankings

        let output = ""
        output += `Rankings for season ${season} as of ${new Date(data.seasons[season].last_fetched_epoch).toString()}\n`
        if(!rankings?.length) {
            output += `No teams detected for season ${season}\n`
        }
        for(let i=0; i<rankings?.length || 0; i++) { //iterating through teams
            let team_wins = 0
            let team_losses = 0
            const games_for_team = await get_teams_games(season, rankings[i].name)
            for(const team_game of games_for_team) {
                if(!team_game.completed) continue

                if(team_game.homeTeam==rankings[i].name && team_game.homePoints>team_game.awayPoints) team_wins++
                if(team_game.awayTeam==rankings[i].name && team_game.awayPoints>team_game.homePoints) team_wins++

                if(team_game.homeTeam==rankings[i].name && team_game.homePoints<team_game.awayPoints) team_losses++
                if(team_game.awayTeam==rankings[i].name && team_game.awayPoints<team_game.homePoints) team_losses++

            }
            output += `${i+1}: ${rankings[i].name} ${team_wins}-${team_losses} (${rankings[i].talent_score.toLocaleString()})\n`
        }

        return output
    })
}
function find_last_relevant(season) {
    return do_with_data(data_filepath, async (data) => {
        const rankings = data.seasons[season].rankings
        const median_num_games = await season_median_num_games(season)

        let output = `Last relevant team\n`
        if(!rankings?.length || rankings.length<25) {
            output+="Not enough teams to make this computation\n"
            return output
        }

        const final_ranked = rankings[24]
        for(let i=rankings.length-1; i>=0; i--) {
            if(final_ranked.talent_score/median_num_games < rankings[i].talent_score) {
                output+= `${i+1}: ${rankings[i].name} (${rankings[i].talent_score.toLocaleString()})\n`
                return output
            }
        }
    })
}

async function main() {
    await do_with_data(keys_filepath, (keys) => {
        client.setConfig({
            headers: {
                Authorization: `Bearer ${keys.CFBD_API_KEY}`
            }
        })
    })

    await do_with_data(data_filepath, async (data) => {
        await fetch_latest()
        const current_date = new Date()
        const current_season = current_date.getFullYear()

        const current_needs_recalculation = data.seasons[current_season].score_sum != data.seasons[current_season].last_calculated_score_sum
        await calculate_rankings_for_season(current_season)
        const current_season_str = `${await rankings_to_string(current_season)}\n${await find_last_relevant(current_season)}`
        console.log(current_season_str)
        if(current_needs_recalculation) await discord_send(current_season_str)

        if(current_date.getMonth() < 2) {
            const last_needs_recalculation = data.seasons[current_season-1].score_sum != data.seasons[current_season-1].last_calculated_score_sum
            await calculate_rankings_for_season(current_season - 1)
            const last_season_str = `${await rankings_to_string(current_season-1)}\n${await find_last_relevant(current_season-1)}`
            console.log(last_season_str)
            if(last_needs_recalculation) await discord_send(last_season_str)
        }
    })
}

main().then(() => console.log("\nDone."))